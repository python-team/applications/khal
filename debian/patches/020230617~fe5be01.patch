Description: fix deleting of multiple instances
 When deleting multiple instances of the same recurring event,
 ikhal would crash because of mismatching etags.
 This would happen, because we delete one instance after the next,
 each time updating the etag.
 As we check for matching etags when deleting
 (the one in the database and the one stored in ikhal),
 this would crash ikhal.
 Matching the etag makes sense,
 as we might overwrite freshly synced data otherwise.
 For now, the server wins.
Author: Christian Geier <geier@lostpackets.de>
Origin: upstream, https://github.com/pimutils/khal/commit/fe5be01
Last-Update: 2023-07-21
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/khal/ui/__init__.py
+++ b/khal/ui/__init__.py
@@ -1123,12 +1123,20 @@
 
     def cleanup(self, data):
         """delete all events marked for deletion"""
+        # If we delete several recuids from the same vevent, the etag will
+        # change after each deletion. As we check for matching etags before
+        # deleting, deleting any subsequent recuids will fail.
+        # We therefore keep track of the etags of the events we already
+        # deleted.
+        updated_etags = {}
         for part in self._deleted[ALL]:
             account, href, etag = part.split('\n', 2)
             self.collection.delete(href, etag, account)
         for part, rec_id in self._deleted[INSTANCES]:
             account, href, etag = part.split('\n', 2)
-            self.collection.delete_instance(href, etag, account, rec_id)
+            etag = updated_etags.get(href) or etag
+            event = self.collection.delete_instance(href, etag, account, rec_id)
+            updated_etags[event.href] = event.etag
 
     def keypress(self, size, key):
         binds = self._conf['keybindings']

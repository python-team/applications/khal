#!/usr/bin/make -f

export PYBUILD_DESTDIR_python3=debian/khal
export PYBUILD_NAME=khal

# resolve upstream version from upstream-provided file (not VCS)
# needed as part of avoiding dependency on python3-setuptools-scm
VERSION = $(shell grep -Po -m1 '^0\.\d+\.\d+$$' CHANGELOG.rst)

cmd = khal
_ENV = $(strip \
 PYTHONDONTWRITEBYTECODE=1 \
 PATH="$(CURDIR)/debian/khal/usr/bin:$(PATH)" \
 PYTHONPATH="$(CURDIR)/debian/khal/usr/lib/$(shell py3versions -d)/dist-packages")

%:
	dh $@ --buildsystem=pybuild

# generate upstream version.py during build
# needed as part of avoiding dependency on python3-setuptools-scm
execute_before_dh_auto_configure execute_before_dh_auto_clean:
	printf '# coding: utf-8\nversion = "%s"\nversion_tuple = (%s, %s, %s)\n' $(VERSION) $(wordlist 1,3,$(subst ., ,$(VERSION))) > khal/version.py

# do upstream cleanup only with patches applied
# needed as part of avoiding dependency on python3-setuptools-scm
override_dh_auto_clean:
	grep -q use_scm_version setup.py || dh_auto_clean --buildsystem=pybuild

execute_after_dh_auto_build:
	PYTHONPATH=. python3 -m sphinx -b man doc/source $(CURDIR)/doc/_build/man

execute_after_dh_auto_install:
# generate shell completion scripts
	$(_ENV) \
	  _KHAL_COMPLETE=bash_source $(cmd) \
		> debian/bash-completion
	$(_ENV) \
	  _KHAL_COMPLETE=zsh_source $(cmd) \
		> debian/_$(cmd)
# prepare sample config for installation using dh_installexamples
	cp -f khal.conf.sample debian/khal.conf

override_dh_sphinxdoc:
ifeq (,$(findstring nodocs, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH=. python3 -m sphinx -b html doc/source $(CURDIR)/debian/khal-doc/usr/share/doc/khal-doc/html
	dh_sphinxdoc
	# Remove unwanted license file, we already reference d/copyright
	rm -f $(CURDIR)/debian/khal-doc/usr/share/doc/khal-doc/html/_sources/license.txt
	rm -f $(CURDIR)/debian/khal-doc/usr/share/doc/khal-doc/html/_sources/license.rst.txt
endif

override_dh_auto_test:
	mkdir -p debian/tmp/locale/
	localedef -f UTF-8 -i en_US ./debian/tmp/locale/en_US.UTF-8/
	localedef -f UTF-8 -i de_DE ./debian/tmp/locale/de_DE.UTF-8/
	localedef -f UTF-8 -i cs_CZ ./debian/tmp/locale/cs_CZ.UTF-8/
	localedef -f UTF-8 -i el_GR ./debian/tmp/locale/el_GR.UTF-8/
	LOCPATH=$(CURDIR)/debian/tmp/locale/ LC_ALL=en_US.UTF-8 dh_auto_test

execute_before_dh_installchangelogs:
	set -e;\
	for i in $$(find doc/source/news -name "khal*.rst" | sort -Vr); do \
		cat $$i;\
		echo;\
	done > changelog.rst
